lazy val akkaHttpVersion = "10.1.10"
lazy val akkaVersion     = "2.5.25"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "com.mesfin",
      scalaVersion    := "2.12.8"
    )),
    name := "keycloak-akka-http",
    libraryDependencies ++= Seq(
      "org.keycloak"      % "keycloak-core"         % "4.0.0.Final",
      "org.keycloak"      % "keycloak-adapter-core" % "4.0.0.Final",
      "org.jboss.logging" % "jboss-logging"         % "3.3.0.Final",
      "org.apache.httpcomponents" % "httpclient"    % "4.5.1",
      
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.0.5"         % Test
    )
  )
